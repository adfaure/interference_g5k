{
  hostPkgs ?  import <nixpkgs> {},
  pkgsMaster ? import (fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/68801b40d60d2f760c925115591a0bd13c01467b.tar.gz")
  {},
  kapack ? import (fetchTarball {
    name = "kapack";
    url = "https://github.com/oar-team/kapack/archive/f70b1149a9ae30fafe87af46e517985bacc331c3.tar.gz";
    sha256 = "sha256:007xyvrcfl9qfi7947kv3clbb7wjniicn1ks6wf2dlcpd2qzmh22";
  }) { }
}:
with kapack;
let
  environments = rec {
    inherit kapack pkgs;
    # Environments for the experimentations

    # Python envs for jupyter
    kernelEnv = pkgs.callPackages ./shells/jupyter-kernels.nix {
      kapack = kapack;
      pkgs = pkgsMaster;
      simgrid = simgrid324;
    };

    rEnv = pkgs.callPackages ./shells/rEnv.nix { pkgs = hostPkgs; };

    # package from this repo, starting to go out of kapack
    gemmpi_migrate = pkgs.callPackage ./gemmpi {
      openmpi = pkgs.openmpi;
      simgrid = simgrid324;
    };

    interference_host_env = pkgs.callPackage ./gemmpi/interference.nix {
      gemmpi = gemmpi_migrate;
      openmpi = pkgs.openmpi;
      mojitos = kapack.mojitos;
    };
};
in
  environments
