{ simgrid, kapack, pkgs }:

with kapack;
let

  jupyter = import (builtins.fetchGit {
    url = https://github.com/tweag/jupyterWith;
    rev = "";
  }) {};

  # Script to transform the traces of gemmpi ranks into a csv summing up all phases
  my_r_scripts = pkgs.stdenv.mkDerivation {
    name = "scripts";
    propagatedBuildInputs = with pkgs.rPackages; [ tidyverse viridis ];
    installPhase = ''
      mkdir -p $out/bin
      cp -r scripts/* $out/bin
    '';
    src = ./../../analysis;
  };

  kaPython = jupyter.kernels.iPythonWith {
    name = "kapython";
    python3 = pkgs.python3;
    packages = p: with p; [
      ipython
      aiofiles
      pandas
      numpy
      ruamel_yaml
      matplotlib
      pkgs.python37Packages.ortools

      # rpy2 + deps
      tzlocal
      simplegeneric
      (rpy2.overrideAttrs(attrs : {
        buildInputs = attrs.buildInputs ++ (with pkgs.rPackages; [ tidyverse viridis ]);
      }))
    ];
  };

  jupyterEnvironment =
    jupyter.jupyterlabWith {
      kernels = [
        kaPython
      ];
  };

in
pkgs.mkShell {
   buildInputs = with pkgs; [
      (pkgs.rWrapper.override {
        packages = with rPackages; [tidyverse viridis];
      })
      my_r_scripts
      simgrid
      kapack.pajeng
      kapack.batsim
      kapack.batsched
      kapack.batexpe
      jupyterEnvironment
    ];
}
