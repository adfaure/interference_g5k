{ pkgs }:


let
  r_pkgs = with pkgs.rPackages; [
    # add r packages here
    Rcpp
    fmsb
    bookdown
    devtools
    ggrepel
    GGally
    yaml
    optparse
    knitr
    rmarkdown
    plyr
    plotly
    stringi
    gtools
    rgl
    readr
    tidyverse
    reshape
    reshape2
    dplyr
    ggplot2
    devtools
    fitdistrplus
    jsonlite
    bookdown
    viridis
    lubridate
    rmarkdown
    servr
    hms
    rhdf5
    h5
    network
    ggnetwork
    forecast
    tidyquant
    timetk
    sweep
    IRanges
    interval
    fuzzyjoin
    # required for knitr export
    caTools
    bitops
  ];
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    # simgrid
    pandoc
    # pkgs.R
    pkgs.unzip
    pkgs.gnutar
    pkgs.hdf5

    (pkgs.rstudioWrapper.override {
      packages = r_pkgs;
    })

  ];
}
