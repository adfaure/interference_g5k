{ openblas }:

let blas64 = null; in
let blas64_ = blas64; in
let
  # To add support for a new platform, add an element to this set.
  configs = {
    # For my computer
    x86_64-linux = {
      BINARY = "64";
      TARGET = "ATHLON";
      DYNAMIC_ARCH = "1";
      CC = "gcc";
      # I disable openmp so I can ensure that  the application is single threaded.
      USE_OPENMP ="0";
    };
  };

  config =
    configs.${stdenv.hostPlatform.system}
    or (throw "unsupported system: ${stdenv.hostPlatform.system}");

  blas64 =
    if blas64_ != null
      then blas64_
      else hasPrefix "x86_64" stdenv.hostPlatform.system;

  custom_openblas = (openblas.overrideDerivation (attrs: rec {
    platform = stdenv.hostPlatform.system;
    preConfigure="echo bro: $platform";
    makeFlags =
      [
        "FC=gfortran"
        ''PREFIX="''$(out)"''
        "USE_THREAD=0"
        "NUM_THREADS=64"
        "INTERFACE64=${if blas64 then "1" else "0"}"
        "NO_STATIC=1"
      ] ++ stdenv.lib.optional (stdenv.hostPlatform.libc == "musl") "NO_AFFINITY=1"
      ++ stdenv.lib.mapAttrsToList (var: val: var + "=" + val) config;
  }));
in
  custom_openblas
