{ pkgs, stdenv, fetchgit, openmpi, automake, clang, openblas, ninja, simgrid }:

stdenv.mkDerivation rec {
  name = "gemmpi-${version}";
  version = "dev";

  src = fetchgit {
    url = "https://gitlab.inria.fr/adfaure/gemmpi";
    sha256 = "sha256:06z0asv1bfcr912clgjlr4vivi6fkss1i9jlm1zzx7dsxzc85n75";
  };

  nativeBuildInputs = [ clang openblas ninja ];

  buildInputs = [ openmpi simgrid ];

  buildPhase = ''
    mpicc --version
    mpirun --version
    ninja
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp -r gemmpi   $out/bin/gemmpi
    cp -r gemsmpi  $out/bin/gemsmpi
    cp -r senrec.mpi $out/bin/senrec.mpi
    cp -r pdgemm_stress $out/bin/dgemm_stress
  '';

  meta = with stdenv.lib; {
    longDescription = ''
      Performs distributed pdgemm, the algorithm is the direct apadtation of the outer product
      discribed in Parallel Algorithms (Chapman & Hall/CRC Numerical Analysis and Scientific Computing Series).
    '';
    description = ''
      Matrix multiplication benchmark on MPI.
    '';
    homepage = "https://gitlab.inria.fr/adfaure/gemmpi";
    license = licenses.gpl3;
    platforms = platforms.unix;
    broken = false;
  };

}
