#!/usr/bin/env bash

# Execute all argument as it were a bash command
nohup ${@:1}

# Save and echo PID
COMMAND_PID=$!
echo "${COMMAND_PID}"
