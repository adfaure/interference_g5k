{
  kapack ? import
   (fetchTarball "https://github.com/oar-team/kapack/archive/master.tar.gz")
   {},
}:
let
  pkgs = kapack.pkgs;
in
pkgs.mkShell rec {
  buildInputs = with pkgs; [
    (python3.withPackages (ps: with ps; with python3Packages; [ docopt ]))
  ];
}
