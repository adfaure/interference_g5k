#!/usr/bin/env bash
# This script is part of the experiment for interferences.

# $1: address of a host to access with SSH
# $2: path of the folder containing the ssh keys to deploy on the machines.

set -eux

HOST=$1
KEYS=$2

ssh -o "StrictHostKeyChecking=no" root@${HOST} 'hostname'

# change root bash
ssh root@${HOST} 'usermod --shell /usr/bin/bash root'

ssh root@${HOST} 'apt update && apt install -y --force-yes sshpass'

if ssh root@${HOST} 'id "g5k"' >/dev/null 2>&1
then
  echo "g5k" user already exist
else
  #  create_user:
  ssh root@${HOST} 'useradd --create-home --password $(openssl passwd -crypt g5k) -G sudo g5k -s /usr/bin/bash'
fi

# allow sudo without password
ssh root@${HOST} 'echo "g5k ALL=(ALL) NOPASSWD: ALL" | sudo EDITOR="tee -a" visudo'

# install_nix as g5k user
ssh root@${HOST} 'echo 1 > /proc/sys/kernel/unprivileged_userns_clone'

# Configure the key on the hst
ssh root@${HOST} mkdir -p /home/g5k/.ssh
scp -r ${2}/* root@${HOST}:/home/g5k/.ssh

ssh root@${HOST} 'chown -R g5k:g5k /home/g5k/.ssh'
ssh root@${HOST} 'chmod 600 /home/g5k/.ssh/id_rsa.pub'
ssh root@${HOST} 'chmod 400 /home/g5k/.ssh/id_rsa'

# Installing dependencies
ssh g5k@${HOST} -i ${2}/id_rsa << EOF
curl https://nixos.org/nix/install | sh
# Sometimes, the first installation fails, I hope two times fixes the issue
curl https://nixos.org/nix/install | sh
echo ". /home/g5k/.nix-profile/etc/profile.d/nix.sh" > /home/g5k/.bashrc
EOF

ssh g5k@${HOST} -i ${2}/id_rsa << EOF
rm -r batmet
nix-env -iA nixpkgs.tcpkali nixpkgs.git
git clone git@gitlab.inria.fr:adfaure/batmet.git
# Installing cachix
nix-env -iA cachix -f https://cachix.org/api/v1/install
cachix use g5k-interference
# keep track for debugging
nix-env -f ~/batmet/environments -iA interference_host_env 2> /tmp/interference_host_env.err
EOF

ssh root@${HOST} 'chmod 400 /home/g5k/batmet/experiments/interferences/experiment_keys/id_rsa'
#Finally, we set monitoring restriction to the lowest security level
ssh root@${HOST} 'echo -1 > /proc/sys/kernel/perf_event_paranoid'
