import logging
import ipaddress
import subprocess
from remote_process import RemoteProcess
from subprocess import PIPE
import yaml

class NetworkGroup:
    # name: str
    # subnet: instance of Ipv4Network
    # g5k_hosts: str list of ip of g5k hosts (exemple: ["paravance-14", "paravance-1.grenoble.grid5000.fr"])
    def __init__(self, name, subnet, g5k_hosts, router_host, router_iface):
        self.name = name
        self.g5k_hosts = g5k_hosts
        self.subnet = subnet
        self.network_size = len(g5k_hosts)
        self.ip_generator = subnet.hosts()
        self.g5k_to_ip = dict()
        self.ip_to_g5k = dict()
        self.router_ip = next(self.ip_generator)
        self.router_host = router_host
        self.router_iface = router_iface
        self.address_list = []
        for g5k_host in g5k_hosts:
            ip = next(self.ip_generator)
            self.address_list.append(ip)
            self.ip_to_g5k[str(ip)] = g5k_host
            self.g5k_to_ip[g5k_host] = str(ip)


    def get_state(self):
        state = dict()
        state["nodes"] = []

        for node in self.g5k_hosts:
            state["nodes"].append({'g5k': node, 'ip': self.g5k_to_ip[node]})

        state["router"] = dict()
        state["router"]["g5k"] = self.router_host
        state["router"]["ip"] = str(self.router_ip)
        state["router"]["iface"] = self.router_iface

        return state

    def log(self):
        logging.info("-------- %s ---------"%self.name)

        for g5k_host in self.g5k_hosts:
            logging.info("%s <-> %s"% (g5k_host, self.g5k_to_ip[g5k_host]))

        logging.info("Ip router: %s on (host:iface)::(%s:%s) " %
                (str(self.router_ip), self.router_host, self.router_iface))

    def configure_router_ip(self):
        # Enable ip forwarding
        logging.info("Configuring router node")
        subprocess.run(["ssh", "root@%s" % self.router_host,
            "echo 1 > /proc/sys/net/ipv4/ip_forward"])

        subprocess.run(["ssh", "root@%s" % self.router_host,
            "ip link set %s up" % self.router_iface])

        ip_addr_command = "ip addr add %s/%s dev %s" % (self.router_ip,
                    self.subnet.netmask, self.router_iface)

        logging.info("Router set: %s" % ip_addr_command)
        subprocess.run(["ssh", "root@%s" % self.router_host, ip_addr_command])

    def configure_nodes_ip(self):
        for node in self.g5k_hosts:
            logging.info("Configuring %s" % node)
            subprocess.run(["ssh", "root@%s" % node, "ip link set eno2 up"])
            subprocess.run(["ssh", "root@%s" % node, "ip addr flush dev eno2"])
            subprocess.run(["ssh", "root@%s" % node,
                "ip addr add %s/%s dev eno2" % (self.g5k_to_ip[node],
                    self.subnet.netmask) ])

    def configure_routes(self, network_group):
        # Command for each host
        # We add a route from the second group passing through our router
        command_route = "ip route add {} via {} dev eno2".format(
                str(network_group.get_network()),
                str(self.router_ip))

        for node in self.g5k_hosts:
            logging.info(command_route)
            subprocess.run(["ssh", "root@%s" % node, command_route])

    def execute_on_group(self, command, key="experiment_keys/id_rsa",
        include_router = False):
        for node in self.g5k_hosts:
            subprocess.run(["ssh", "-i", key, "g5k@%s" % node, command])

        if include_router:
            router = self.access_router(command, key = key)
            print(router.stdout)

    def execute_on_node(self, node, command, key="experiment_keys/id_rsa",
        include_router = False):
        if node in self.g5k_hosts:
            subprocess.run(["ssh", "-i", key, "g5k@%s" % node, command])
        else:
            logging.error("node %s note in group" % node)

    def access_router(self, command, key="experiment_keys/id_rsa"):
        passerelle = self.g5k_hosts[0]
        wrapped_command = "ssh %s %s" % (self.router_ip, command)
        return subprocess.run(["ssh", "-i", key, "g5k@%s" % passerelle,
            wrapped_command], stdout = PIPE, stderr = PIPE)

    def access_node_through_router(self, command, hostame, extra_ssh_params = [],
            key="experiment_keys/id_rsa"):
        passerelle = self.router_host
        key_on_router = "/home/g5k/.ssh/id_rsa"
        wrapped_command = "ssh -i %s %s %s" % (key_on_router, self.router_ip, command)
        print(extra_ssh_params)
        ssh_array = [ "ssh" ] + extra_ssh_params
        return subprocess.run(ssh_array + [ "-i", key, "g5k@%s" % passerelle,
            wrapped_command], shell = True, stdout = PIPE, stderr = PIPE)

    def access_ip_through_router(self, command, ip, extra_ssh_params = [],
            key="experiment_keys/id_rsa"):

        p = RemoteProcess(ip, command, gateway = self.router_host,
                extra_ssh_opts = extra_ssh_params)

        return p.run()

    # will use the router to shut all eno1 interfaces
    def isolate_network(self, key="experiment_keys/id_rsa"):
        start_if_cmd = "sudo ip link set eno1 down"
        for ip in self.address_list:
            router = self.access_ip_through_router(
                     start_if_cmd, ip, extra_ssh_params = ["-f"], key = key
            )

    def expose_network(self, key="experiment_keys/id_rsa"):
        shutdown_if_cmd = "sudo ip link set eno1 up"
        for ip in self.address_list:
            router = self.access_ip_through_router(
                     shutdown_if_cmd, ip, extra_ssh_params = ["-f"], key = key
            )

    def get_corresponding_ips(self, host_list):
        ip_list= []
        for host in host_list:
            if host in self.g5k_hosts:
                ip = self.get_ip_for_host(host)
                ip_list.append(ip)
            else:
                logging.warning("provided host %s is not in group %s" % (host,
                    self.name))

        return ip_list

    def get_network(self):
        return self.subnet

    def get_router_addr(self):
        return self.router_ip

    def get_ip_for_host(self, hostname):
        return self.g5k_to_ip[hostname]

    def get_host_for_ip(self, hostname):
        return self.ip_to_g5k[hostname]

    def get_address_list(self):
        return self.address_list

    def get_g5k_host(self):
        return self.g5k_hosts

    def get_router_g5k_hostname(self):
        return self.router_host

    def get_network_size(self):
        return self.network_size
