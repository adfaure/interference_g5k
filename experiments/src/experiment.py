import copy
import yaml
import time
import uuid
import os
from networkgroup import NetworkGroup
from remote_process import RemoteProcess
import subprocess
from subprocess import PIPE
import logging
import datetime
import random

def kill_remote_process(node, pid, sig = 15, key="experiment_keys/id_rsa"):
    res = subprocess.run(["ssh", "-i", key, "g5k@%s" % node, "kill -%d %s" %
        (sig, pid)],
        capture_output=True)

def monitor_node_process(node, key="experiment_keys/id_rsa", gateway = None, freq = 1, netface = None,
        performance_counters = ["cpu_cycles", "instructions"],
        outputfile = "./mojitos.csv"):

    # Base command
    command_params = [ "mojitos",
            "-p", ",".join(performance_counters),
            "-f", str(freq),
            "-o", outputfile
            ]

    # Add network monitoring if activated
    if netface is not None:
        command_params += ["-d", netface]
    mojitos_command = " ".join(command_params)

    if gateway is not None:
        return RemoteProcess(node, mojitos_command, gateway = gateway)
    else:
        return RemoteProcess(node, mojitos_command)


class Experiment:
    """
    """

    def __init__(self, configuration_file, netgroup_1, netgroup_2, root_folder
            = "/home/afaure/batmet_experiments", key = "experiment_keys/id_rsa"):
        # Inputs
        with open(configuration_file) as f:
            # use safe_load instead load
            self.in_data = yaml.safe_load(f)
        print(self.in_data)

        self.nb_host_per_group = int(self.in_data["hosts_per_netgroup"])
        self.key = key
        self.root_folder = root_folder
        self.group_1 = netgroup_1
        self.group_2 = netgroup_2

        # Selecting the number of host we need
        self.g1_hosts = netgroup_1.get_g5k_host()[:self.nb_host_per_group]
        self.g1_ips = netgroup_1.get_corresponding_ips(self.g1_hosts)

        self.g2_hosts = netgroup_2.get_g5k_host()[:self.nb_host_per_group]
        self.g2_ips = netgroup_2.get_corresponding_ips(self.g2_hosts)

        self.router = self.group_1.get_router_g5k_hostname()

        # allocating machine for tcpkali
        self.tcpkali_host_per_group = int(
                self.in_data["tcpkali"]["hosts_per_group"])
        self.tcpkali_g1 = self.g1_hosts[:self.tcpkali_host_per_group]
        self.tcpkali_g2 = self.g2_hosts[:self.tcpkali_host_per_group]
        self.tcpkali_processes = []

        # allocating machine for mpi
        self.mpi_host_per_group = int(
                self.in_data["mpi"]["hosts_per_group"])

        offset = len(self.g1_hosts) - self.mpi_host_per_group
        self.mpi_g1 = self.g1_hosts[offset:]
        self.mpi_g2 = self.g2_hosts[offset:]

        # Instance
        # Generate an unique name for this run
        self.run_name = str(uuid.uuid4())[:8]
        self.is_finished = False

        # Folder in which files will be generated
        self.remote_folder_name = os.path.join(
                "/home/g5k",
                self.in_data["instance_name"],
                self.run_name)

        self.local_folder_name = os.path.join(
                root_folder,
                self.in_data["instance_name"],
                self.run_name)

        self.out_data = {
                "network" : {
                    "instance_hosts" : {}
                    },
                "tcpkali": {},
                "mpi": {},
                }

    def clean_host_before_start(self):
        logging.debug("clean mojitos")
        self.execute_on_machines(self.g1_ips, "pkill mojitos", wait = False,
                use_router = True)
        self.execute_on_machines(self.g2_ips, "pkill mojitos", wait = False,
                use_router = True)
        self.execute_on_router("pkill mojitos")

        logging.debug("clean mpi")
        self.execute_on_machines(self.g1_ips, "pkill mpirun", wait = False,
                use_router = True)
        self.execute_on_machines(self.g2_ips, "pkill mpirun", wait = False,
                use_router = True)
        self.execute_on_router("pkill mpirun")

        logging.debug("clean tcpkali")
        self.execute_on_machines(self.g1_ips, "pkill periods", wait = False,
                use_router = True)
        self.execute_on_machines(self.g2_ips, "pkill periods", wait = False,
                use_router = True)
        self.execute_on_machines(self.g1_ips, "pkill tcpkali", wait = False,
                use_router = True)
        self.execute_on_machines(self.g2_ips, "pkill tcpkali", wait = False,
                use_router = True)

    def save(self):
        netg1_state = self.group_1.get_state()
        netg2_state = self.group_2.get_state()

        self.out_data["instance_name"] = self.in_data["instance_name"]
        self.out_data["run_id"] = self.run_name

        self.out_data["network"]["net_g1"] =  netg1_state
        self.out_data["network"]["net_g2"] =  netg2_state
        self.out_data["network"]["instance_hosts"] = dict()
        self.out_data["network"]["instance_hosts"]["g1"] = self.g1_hosts
        self.out_data["network"]["instance_hosts"]["g2"] = self.g2_hosts

        self.out_data["tcpkali"]["g1"] = self.tcpkali_g1
        self.out_data["tcpkali"]["g2"] = self.tcpkali_g2

        self.out_data["mpi"]["g1"] = self.mpi_g1
        self.out_data["mpi"]["g2"] = self.mpi_g2

        self.output_sate_file = os.path.join(self.local_folder_name, "out.yaml")

        with open(self.output_sate_file, 'w') as outfile:
            yaml.safe_dump(self.out_data, outfile, default_flow_style=False)

        self.output_sate_file = os.path.join(self.local_folder_name, "in.yaml")
        with open(self.output_sate_file, 'w') as outfile:
            yaml.safe_dump(self.in_data, outfile, default_flow_style=False)

    def execute_on_machines(self, machines, command, use_router = False,
            wait = True, key = "experiment_keys/id_rsa"):
        if use_router is True:
            for machine in machines:
                if wait:
                    r = RemoteProcess(machine, command,
                        gateway = self.router,
                        access_key = self.key)
                    r.run()
                else:
                    r = RemoteProcess(machine, command,
                        gateway = self.router, extra_ssh_opts = ["-f"],
                        access_key = self.key)
                    r.run()

        else:
            for machine in machines:
                r = RemoteProcess(machine, command , access_key = self.key)
                if wait:
                    r.run()
                else:
                    r.run_bg()

    def execute_on_router(self, command, wait = True):
        r = RemoteProcess(self.router, command, access_key = self.key)
        if wait:
            r.run()
        else:
            r.run_bg()

    def clean_monitoring(self):
        # Create a folder on for the exp on each hosts
        logging.info("Cleaning mojitos processes")
        for task in self.monitoring_processes:
            task.terminate()

    def gather_files(self):
        # TODO: Gather router files
        # We use the "instance folder" name which should be the parent of all
        # run from this instance
        instance_folder = os.path.join(self.root_folder,
                self.in_data["instance_name"])

        tar_tmpl = "tar cvf {archive}.tar -C {folder} ."
        self.execute_on_machines(self.g1_ips,
                tar_tmpl.format(archive=self.run_name+".$(hostname)",
                    folder = self.remote_folder_name),
                    use_router = True)

        self.execute_on_machines(self.g2_ips,
                 tar_tmpl.format(archive=self.run_name+".$(hostname)", folder =
                    self.remote_folder_name),
                    use_router = True)

        self.execute_on_router(tar_tmpl.format(
                            archive=self.run_name+".$(hostname)",
                            folder = self.remote_folder_name,
                            remote_dest = self.remote_folder_name))

        for host in self.g1_ips + self.g2_ips :
            addr = "g5k@%s" % host
            path = ":" + self.run_name + ".*.tar"
            proxy_jump = [ "-o",
                    'ProxyCommand="ssh -i {} -W %h:%p g5k@{}"'.format(self.key,
                     self.router)]
            exec_array = [ "scp", "-i", self.key ] + proxy_jump + \
                         [ addr + path, self.local_folder_name ]

            t = subprocess.run(" ".join(exec_array),
                    shell = True, stderr = PIPE, stdout = PIPE)

        router_addr = "g5k@%s" % self.router
        router_path = ":" + self.run_name + "."  + self.router + ".tar"
        subprocess.run(["scp", "-i", self.key, router_addr + router_path,
            self.local_folder_name])

    def get_timestamp_monotonic_matching(self):
        command = "python3 -c \"import time;print(\\\"{};{}\\\".format(time.time(),time.monotonic()))\""
        self.out_data["time_matching"] = dict()
        for host in self.g1_ips + self.g1_ips + [self.router]:

            res = RemoteProcess(host, command, access_key = self.key,
                    remote_stdout = "/tmp/myclock",
                    remote_stderr = "/tmp/myclock.err",
                    gateway = self.router).run().get_stdout()

            times = res.split(";")
            self.out_data["time_matching"][host] = {
                    "timestamp": float(times[0]),
                    "monotonic": float(times[1]) }

    def generate_hostfile(self):
        mpi_data = copy.deepcopy(self.in_data["mpi"])
        ip_list = []
        for host in self.mpi_g2:
            ip_list.append(self.group_2.get_ip_for_host(host))

        for host in self.mpi_g1:
            ip_list.append(self.group_1.get_ip_for_host(host))

        hostfile = ""
        for ip in ip_list:
            hostfile += "{} slots={}\n".format(ip, mpi_data["procs_per_host"])

        self.hostfile = hostfile

    def generate_rand_hostfile(self):
        mpi_data = copy.deepcopy(self.in_data["mpi"])
        ip_list = []
        for host in self.mpi_g2:
            ip_list.append(self.group_2.get_ip_for_host(host))

        for host in self.mpi_g1:
            ip_list.append(self.group_1.get_ip_for_host(host))

        hostfile = ""
        cores_list = []
        for ip in ip_list:
            # hostfile += "{} slots={}\n".format(ip, mpi_data["procs_per_host"])
            for i in range(int(mpi_data["procs_per_host"])):
                cores_list.append(ip)

        random.Random(42).shuffle(cores_list)
        hostfile = "\n".join(cores_list)
        self.hostfile = hostfile + "\n"

    def prepare_mpi(self):
        mpi_data = copy.deepcopy(self.in_data["mpi"])
        self.hostfile_path = os.path.join(self.local_folder_name, "hostfile")

        with open(self.hostfile_path, 'w') as outfile:
            outfile.write(self.hostfile)

        entry_host = self.mpi_g1[0]
        entry_ip = self.group_1.get_ip_for_host(entry_host)
        logging.info("Node which will run mpi is : %s" % entry_host)

        logging.info("Deploying hostfile")
        addr = "g5k@%s" % entry_ip
        remote_path = "{}:{}".format(addr, self.remote_folder_name)
        proxy_jump = [ "-o",
                    'ProxyCommand="ssh -i {} -W %h:%p g5k@{}"'.format(self.key,
                     self.router)]

        exec_array = ["scp", "-i", self.key] + proxy_jump + \
            [self.hostfile_path, remote_path]

        logging.info(" ".join(exec_array))

        subprocess.run(" ".join(exec_array), shell = True)

        if not "mpi_args" in mpi_data:
            mpi_args = ""
        else:
            mpi_args = mpi_data["mpi_args"]

        mpi_cmd = " ".join([
            "mpirun" ,
            "-hostfile",
            os.path.join(self.remote_folder_name,"hostfile"),
            mpi_args,
            mpi_data["binary"],
            mpi_data["binary_args"],
            "-p {}".format(os.path.join(self.remote_folder_name, "mpi.progress"))
         ])

        self.out_data["mpi"]["command"] = mpi_cmd
        self.out_data["mpi"]["mpirun_on_host"] = entry_host

        logfile = os.path.join(self.remote_folder_name,
                "{}.mpirun".format(entry_host))

        p = RemoteProcess(entry_ip, mpi_cmd,
                gateway = self.router,
                remote_stderr = logfile + ".err",
                remote_stdout = logfile + ".out")

        self.mpi_process = p

    def run_mpi(self, starts_at = None):

        p = self.mpi_process

        logging.info("MPI running: Timout %s" % self.in_data["timeout"])
        timeout =  int(self.in_data["timeout"])
        p.run_bg(starts_at = starts_at)
        p.wait(timeout = timeout)

        self.out_data["mpi"]["runtime"] = str(p.get_runtime())
        self.out_data["mpi"]["start_time"] = str(p.get_starttime())
        self.out_data["mpi"]["end_time"] = str(p.get_endtime())
        self.out_data["mpi"]["reached_timeout"] = str(p.was_timeout_reached())

    def run_tcpkali_client(self, starts_at):
        self.tcpkali_processes_client.run_bg(starts_at = starts_at)

    def run_tcpkali_server(self):
        self.tcpkali_processes_server.run_bg()

    def isolate_networks(self):
        self.group_1.isolate_network()
        self.group_2.isolate_network()

    def expose_networks(self):
        self.group_1.expose_network()
        self.group_2.expose_network()

    def stop_tcpkali(self):
        self.tcpkali_processes_client.terminate()
        self.tcpkali_processes_server.terminate()

    def prepare_tcpkali_server(self):
        logging.info("Running tcpkali")
        ser_kalidata = copy.deepcopy(self.in_data["tcpkali"]["server"])

        server_command_tmpl = [ "tcpkali"]
        for param in ser_kalidata:
            server_command_tmpl += ["--{}".format(param),
                    str(ser_kalidata[param])]

        server_command_tmpl += ["--duration", "{}".format(self.in_data["timeout"])]

        # Server host
        ser_ip = self.group_2.get_ip_for_host(self.tcpkali_g2[0])
        ser_cmd = " ".join(server_command_tmpl)

        self.out_data["tcpkali"]["server_command"] = ser_cmd

        logging.info("tcpkali running")
        logfile = os.path.join(self.remote_folder_name, "{}.tcpkali")

        self.tcpkali_processes_server = RemoteProcess(ser_ip,
                    ser_cmd,
                    gateway = self.router,
                    remote_stderr = logfile.format(self.tcpkali_g2[0]) + ".err",
                    remote_stdout = logfile.format(self.tcpkali_g2[0]) + ".out")

    def prepare_tcpkali_client(self):
        logging.info("Running tcpkali")
        cli_kalidata = copy.deepcopy(self.in_data["tcpkali"]["client"])
        # ./periods tidle 5 tcmd 10 -e 1000.e -o 1000.o -g D -- tcpkali -m 0 127.0.0.1:8080 -T 5 --connections 10

        is_periodic = "periods" in cli_kalidata

        client_command_tmpl = [ "tcpkali" ]
        for param in cli_kalidata["args"]:
            if param != "port":
                client_command_tmpl += ["--{}".format(param),
                    str(cli_kalidata["args"][param])]

        # Server host
        ser_ip = self.group_2.get_ip_for_host(self.tcpkali_g2[0])
        client_command_tmpl += ["{}:{}".format(ser_ip,
            cli_kalidata["args"]["port"])]

        cli_cmd = " ".join(client_command_tmpl)


        logging.info("tcpkali running")
        logfile = os.path.join(self.remote_folder_name, "{}.tcpkali")
        periodslogfile = os.path.join(self.remote_folder_name, "{}.periods")

        if is_periodic:
            periodslogfile = os.path.join(self.remote_folder_name, "{}.periods")
            periodic_command_tmpl = "periods tidle {tidle} tcmd {tcmd} -e {stderr} -o {stdout} --duration {duration} -g D -- {command}"
            final_cli_cmd = periodic_command_tmpl.format(
                    tidle = cli_kalidata["periods"]["tidle"],
                    tcmd = cli_kalidata["periods"]["tcmd"],
                    stderr = logfile.format(self.tcpkali_g1[0]) + ".err",
                    stdout = logfile.format(self.tcpkali_g1[0]) + ".out",
                    command = cli_cmd,
                    duration = self.in_data["timeout"]
            )
        else:
            final_cli_cmd = cli_cmd


        self.out_data["tcpkali"]["client_command"] = final_cli_cmd
        cli_ip = self.group_1.get_ip_for_host(self.tcpkali_g1[0])

        self.tcpkali_processes_client = RemoteProcess(
                cli_ip, final_cli_cmd,
                gateway = self.router,
                remote_stdout = periodslogfile.format(self.tcpkali_g1[0]) + ".out",
                remote_stderr = periodslogfile.format(self.tcpkali_g1[0]) + ".err")

    def monitor_hosts(self):
        # Configure the monitoring
        logging.info("create processes for monitoring")
        netfaces = self.in_data["mojitos"]["network"]
        self.monitoring_processes = []

        for netface in netfaces:
          for host in self.g1_ips:
              # Mojitos output
              g5kname = self.group_1.get_host_for_ip(host)
              mojitos_out = os.path.join(self.remote_folder_name,
                      "%s.%s.mojitos.csv" % (g5kname, netface))
              self.monitoring_processes.append(monitor_node_process(host, outputfile =
                mojitos_out, gateway = self.router, netface = netface))

          for host in self.g2_ips:
              # Mojitos output
              g5kname = self.group_2.get_host_for_ip(host)
              mojitos_out = os.path.join(self.remote_folder_name,
                      "%s.%s.mojitos.csv" % (g5kname, netface))
              self.monitoring_processes.append(monitor_node_process(host, outputfile =
                mojitos_out, gateway = self.router, netface = netface))

          logging.info("create processes for monitoring the router")
          mojitos_out = os.path.join(self.remote_folder_name,
                  "%s.%s.mojitos.csv" % (self.router, netface))
          self.monitoring_processes.append(monitor_node_process(self.router, outputfile =
                mojitos_out, gateway = self.router, netface = netface))

        logging.info("Start monitoring processes")
        for task in self.monitoring_processes:
            task.run_bg()

    def run_instance(self):
        logging.info("Prepare local files for instance: %s",
                self.local_folder_name)

        logging.debug("clean host")
        self.clean_host_before_start()

        self.get_timestamp_monotonic_matching()
        os.makedirs(self.local_folder_name)

        logging.info("Shutdown network interface eno1")
        self.isolate_networks()

        # Create a folder on for the exp on each hosts
        logging.info("create folder for exp")
        self.execute_on_machines(self.g1_ips,
                "mkdir -p %s" % self.remote_folder_name, use_router = True)
        self.execute_on_machines(self.g2_ips,
                "mkdir -p %s" % self.remote_folder_name, use_router = True)

        self.execute_on_router("mkdir -p %s" % self.remote_folder_name)

        if self.in_data["run_mpi"]:
            self.generate_rand_hostfile()
            self.prepare_mpi()

        if self.in_data["run_tcpkali"]:
            logging.info("Prepare tcpkali")
            self.prepare_tcpkali_client()
            self.prepare_tcpkali_server()

        self.save()

        logging.info("Start monitoring")
        self.monitor_hosts()
        time.sleep(5)

        starts_at = int(time.time() + 10)

        if self.in_data["run_tcpkali"]:
            logging.info("Start tcpkali")
            self.run_tcpkali_server()
            self.run_tcpkali_client(starts_at = starts_at)

        logging.info("Configure to start mpi and tcpkali (if activated at ): %s"%  starts_at)

        if self.in_data["run_mpi"]:
            logging.info("Run MPI")
            self.run_mpi(starts_at = starts_at)
        else:
            logging.info("running without MPI, wating timeout")
            time.sleep(int(self.in_data["timeout"]))

        time.sleep(int(5))

        if self.in_data["run_tcpkali"]:
            logging.info("Stop tcpkali")
            self.stop_tcpkali()

        time.sleep(int(5))

        logging.info("Clean monitoring")
        self.clean_monitoring()
        logging.info("Gather data")
        self.gather_files()
        self.save()

        logging.info("Exposing network")
        self.expose_networks()
        self.clean_host_before_start()
