import yaml
import copy
import time
import uuid
import os
import subprocess
from subprocess import PIPE
import logging
import datetime
import random

class RemoteProcess:

    def __init__(self, hostname, command,
            remote_stderr = None,
            remote_stdout = None,
            gateway = None,
            extra_ssh_opts = [],
            access_key = "experiment_keys/id_rsa"):

        self.hostname = hostname
        # self.pid = pid
        self.remote_stderr = remote_stderr
        self.remote_stdout = remote_stdout
        self.access_key = access_key
        self.command = command
        self.program = command.split()[0]
        self.extra_ssh_opts = extra_ssh_opts
        # Using a gateway
        self.gateway = gateway
        if self.gateway is not None:
            self.proxy_jump =  [ "-o",
                    'ProxyCommand="ssh -i {} -W %h:%p g5k@{}"'.format(
                        self.access_key,  self.gateway)
                    ]

        # Only for self.run()
        self.runtime = None
        self.ends_at = None
        self.starts_at = None
        self.stdout = None
        self.stderr = None
        self.timeout_reached = False
        self.is_started = False
        self.is_finished = False

    def get_ssh_array(self, extra_ssh_opts = []):
        if self.gateway is not None:
            process_array = [ "ssh", "-i", self.access_key ] + \
                    extra_ssh_opts + self.extra_ssh_opts + \
                    self.proxy_jump + [ "g5k@%s" % self.hostname ]
        else:
            process_array = ["ssh", "-i", self.access_key ] + \
               extra_ssh_opts + self.extra_ssh_opts + \
               [ "g5k@%s" % self.hostname ]

        return process_array

    def wait(self, timeout = None, starts_at = None):
        if self.is_finished:
            return self

        if not self.is_started:
            logging.info("process not running, lauching")
            self.run_bg()

        if not self.check():
            self.is_finished = True
            self.ends_at = datetime.datetime.now()
            self.run_time = self.ends_at - self.starts_at
            return self

        if timeout is not None:
            elapsed = datetime.datetime.now() - self.starts_at
            remaining = timeout - elapsed.seconds

            if elapsed.seconds > timeout and self.check():
                logging.info("timeout reached anyway")
                self.terminate()
                self.timeout_reached = True
            else:
                logging.info("program running, remaining seconds %s " % remaining)

                process_array = self.get_ssh_array() + [
                    "'timeout {} tail --pid={} -f /dev/null'".format(
                    remaining, self.pid) ]

                result = subprocess.run(" ".join(process_array), shell = True,
                        stdout=PIPE, stderr=PIPE)

                self.terminate()

                if result.returncode == 124 or result.returncode == 137:
                    self.timeout_reached = True
                else:
                    self.timeout_reached = False

                return self

        else:
            # Wait for the end of the process using tail
            process_array = self.get_ssh_array() + [ "tail --pid={} -f /dev/null".format(self.pid) ]
            result = subprocess.run(" ".join(process_array), shell = True,
                stdout=PIPE, stderr=PIPE)

            self.is_finished = True
            self.ends_at = datetime.datetime.now()
            self.run_time = esssself.ends_at - self.starts_at

        return self

    def run(self, timeout = None, starts_at = None):
        # print(" ".join(["ssh", "-i", self.key, "g5k@%s" % self.node, nohuped]))
        if self.is_started or self.is_finished:
            logging.error("process already finished or running")
            return

        if timeout is not None:
            return self.wait(timeout)

        if starts_at is not None:
            wae = "/home/g5k/batmet/experiments/interferences/wait_and_exec.sh " + str(starts_at) + " "

            self.command = wae + self.command

        command = self.command
        if self.remote_stdout is not None:
            command += " > %s " % self.remote_stdout
        if self.remote_stderr is not None:
            command += " 2> %s " % self.remote_stderr

        self.is_started = True
        self.starts_at = datetime.datetime.now()

        process_array = self.get_ssh_array() + [ "'" + command + "'"]

        result = subprocess.run([" ".join(process_array)], shell = True, stdout=PIPE, stderr=PIPE)

        self.ends_at = datetime.datetime.now()
        self.run_time = self.ends_at - self.starts_at

        rpid = result.stdout.decode().rstrip()
        self.pid = rpid
        self.is_finished = True
        return self

    def get_stdout(self):

        process_array = self.get_ssh_array() + [ "cat {}".format(self.remote_stdout) ]

        result = subprocess.run(" ".join(process_array), shell = True,
               stdout=PIPE, stderr=PIPE)

        return result.stdout.decode("utf-8").strip()

    def get_endtime(self):
        return self.ends_at

    def get_starttime(self):
        return self.starts_at

    def get_runtime(self):
        return self.run_time

    def check(self):
        if self.is_finished:
            return not self.is_finished

        process_array = self.get_ssh_array() + [  "kill -s 0 %s" % self.pid ]

        result = subprocess.run(" ".join(process_array), shell = True,
            stdout=PIPE, stderr=PIPE)

        return len(result.stderr) == 0

    def was_timeout_reached(self):
        return self.timeout_reached

    def run_bg(self, starts_at = None):
        if self.is_started or self.is_finished:
            logging.error("process already finished or running")
            return self

        if starts_at is not None:
            wae = "/home/g5k/batmet/experiments/interferences/wait_and_exec.sh " + str(starts_at) + " "
            self.command = wae + self.command

        nohuped = self.wrap_nohup(
                self.command, stdout = self.remote_stdout,
                stderr = self.remote_stderr)

        process_array = self.get_ssh_array() + [  nohuped ]

        self.starts_at = datetime.datetime.now()

        result = subprocess.run(" ".join(process_array), shell = True,
            stdout=PIPE, stderr=PIPE)

        rpid = result.stdout.decode().rstrip()
        self.pid = rpid
        self.is_started = True
        return self

    def send_sig(self, sig):
        if self.gateway is not None:
            process_array = ["ssh", "-i", self.access_key ] + \
                self.proxy_jump + \
                [ "g5k@%s" % self.hostname, "kill -%d %s" % (sig, self.pid) ]

        else:
            process_array = ["ssh",  "-i", self.access_key,
                "g5k@%s" % self.hostname, "kill -%d %s" % (sig, self.pid)]

        res = subprocess.run(" ".join(process_array), shell=True,
                capture_output=True)
        logging.debug(res.stdout.decode().rstrip())
        return self

    def kill(self):
        self.send_sig(9)
        if self.check() is True:
            logging.warning("failed to kill %s" % self.pid)
            return self

        self.is_finished = True
        self.ends_at = datetime.datetime.now()
        self.run_time = self.ends_at - self.starts_at
        return self

    def terminate(self):
        self.send_sig(15)
        if self.check() is True:
            logging.warning("failed to kill %s" % self.pid)
            return self

        self.is_finished = True
        self.ends_at = datetime.datetime.now()
        self.run_time = self.ends_at - self.starts_at
        return self

    @staticmethod
    def wrap_nohup(command, stderr = "/dev/null", stdout = "/dev/null"):
        # IDK why, but we need to wrap it with two ' to make it works
     # return "''nohup %s </dev/null >/dev/null 2>&1 & echo $!''" % command
     if stderr is None:
         stderr = "/dev/null"

     if stdout is None:
         stdout = "/dev/null"

     return "'nohup %s </dev/null > %s 2> %s & echo $!'" % (command, stdout, stderr)

