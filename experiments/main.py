#!/usr/bin/env python3

import sys
import logging
import subprocess
import os
import ipaddress
import yaml
import random
import copy
import time
from pathlib import Path

sys.path.append("src")
from networkgroup import NetworkGroup
import experiment

# Installing dependencies on hosts
# Including nix, mpi, tcpkali, git
# In addition one can connect without password using
# `ssh -i experiment_keys/id_rsa g5k@{node}` to any node of the cluster
def setup_g5k_hosts(hostnames, job_id):
    processes = []
    for node in hostnames:
        pass
        logging.info("Installing host={}".format(node))
        install = subprocess.Popen(["./setup_host.sh", node, "experiment_keys"],
                stdout=open(job_id + "_" + node+".log_install.out", 'wb'),
                stderr=open(job_id + "_" + node+".log_install.err", 'wb'))

        processes.append(install)

    for inst in processes:
       inst.communicate()

def configure_network(hostnames, nodes_list):
    logging.info("Network configuration")
    # Picking one node to do the router
    router_node = "{}".format(nodes_list[0])
    logging.info("Router node={}".format(router_node))

    # Gather reserved subnets
    # so instead of using it, we parse the output of -n option.
    subnets_raw = subprocess.getoutput("g5k-subnets -p").split("\n")
    subnets = list(map(lambda subnet:  ipaddress.ip_network(subnet), subnets_raw))

    logging.info("subnets: {}".format(subnets))
    # Creating two groups of nodes
    nodes_jobs = nodes_list[1:]
    if len(nodes_jobs) % 2 != 0:
        logging.warning("Odd number of node: {}".format(str(len(nodes_jobs))))

    g1_nodes = nodes_jobs[0:int(len(nodes_jobs)/2)]
    g2_nodes = nodes_jobs[int(len(nodes_jobs)/2):]

    network_g1 = NetworkGroup("G1", subnets[0], g1_nodes, router_node, "eno1")
    network_g1.log()
    network_g1.configure_nodes_ip()
    network_g1.configure_router_ip()

    network_g2 = NetworkGroup("G2", subnets[1], g2_nodes, router_node, "eno2")
    network_g2.log()
    network_g2.configure_nodes_ip()
    network_g2.configure_router_ip()

    # add route from g1 to g2 and vice-versa
    network_g1.configure_routes(network_g2)
    network_g2.configure_routes(network_g1)

    # Return the networks so we can generate hostname
    return [network_g1, network_g2]

def run_on_host(network, node, command, key="experiment_keys/id_rsa"):
    ip = network.get_ip_for_host()
    logging.info("Run command on host=%s with ip=%s" % (node, ip))
    subprocess.run(["ssh", "-i", key, "g5k@%s" % ip, command])

def save_status(status, file):
    with open(file, 'w') as outfile:
        yaml.safe_dump(status, outfile, default_flow_style=False)

if __name__ == "__main__":
    logging.basicConfig(format='[%(asctime)s]::%(message)s', level=logging.INFO)
    logging.info('Starting experiment')

    save_file = "exp.{}.yaml".format(int(time.time()))
    status = dict()

    key_folder="experiment_keys"

    # Security option for ssh configuration
    os.chmod(key_folder + "/id_rsa", 0o400)
    os.chmod(key_folder + "/id_rsa.pub", 0o600)

    experiment_file = sys.argv[1]
    with open(experiment_file) as f:
        # use safe_load instead load
        experiment_conf = yaml.safe_load(f)
        print(experiment_conf)

    all_instances = []
    for instance in experiment_conf:
        filepath = os.path.join(os.path.dirname(sys.argv[1]), instance["file"])
        if not os.path.exists(filepath):
            logging.critical("instance file %s does not exist" % filepath)
            sys.exit()

        instance["file"] = filepath

        for repeat in range(instance["repeat"]):
            all_instances.append(copy.deepcopy(instance))


    random.Random(42).shuffle(all_instances)
    status["all_instances"] = all_instances

    job_id = os.environ["OAR_JOB_ID"]

    # Get the node list from oar environment variables
    nodes_list = subprocess.getoutput("cat $OAR_NODE_FILE|uniq").split("\n")

    setup_g5k_hosts(nodes_list, job_id)
    networks = configure_network(nodes_list, nodes_list)

    save_status(status, save_file)

    for instance in all_instances:
        if not instance["groupname"] in status:
            status[instance["groupname"]] = dict()
            group = status[instance["groupname"]]

        save_status(status, save_file)
        local_folder = os.path.join( "/home/afaure/batmet_experiments",
                instance["groupname"])

        Path("local_folder").mkdir(parents=True, exist_ok=True)

        exp = experiment.Experiment(instance["file"], networks[0], networks[1],
                root_folder = local_folder)

        if not exp.in_data["instance_name"] in group:
            group[exp.in_data["instance_name"]] = dict()

        group[exp.in_data["instance_name"]][exp.run_name] = "start"
        save_status(status, save_file)
        exp.run_instance()
        group[exp.in_data["instance_name"]][exp.run_name] = "finished"
        save_status(status, save_file)

