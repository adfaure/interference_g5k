# This shell defines a development environment for the Chord project.
{
  env ? import ./environments {}
}:
with env;
pkgs.mkShell rec {
   buildInputs = with pkgs; [
     # R
     env.rEnv.buildInputs
     # Jupyter notebook
     env.kernelEnv.buildInputs
   ];
}
